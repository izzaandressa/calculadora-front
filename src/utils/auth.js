/* eslint-disable no-prototype-builtins */
import queryString from 'query-string';
import jwt from 'jsonwebtoken';
//import { APP_URL, URL_LOGIN } from '../constants/http.constants';

const authHeader = () => {
  const authorization = localStorage.getItem('authorization');

  if (authorization) {
    return { Authorization: `Bearer ${authorization}` };
  }
  return {};
};

const checkIsLoggedIn = () => {
  return !!localStorage.getItem('authorization');
};

const checkAuth = () => {
  if (checkIsLoggedIn()) {
      return true;
    //window.location.href = APP_URL;
  } else {
    return false;
    //window.location.href = URL_LOGIN;
  }
};

const checkAuthorization = () => {
  if (localStorage.getItem('authorization')) {
    const params = new URLSearchParams(window.location.search);

    if (params.has('token')) {
      const previousParams = queryString.parse(window.location.search);
      delete previousParams.token;
      const search = Object.keys(previousParams).reduce((acc, key) => {
        return `${acc}${key}=${previousParams[key]}&`;
      }, '?');

      window.history.pushState({}, document.title, `/${search}`);
    }
  }

  return localStorage.getItem('authorization');
};

const saveAuthorization = (token, profiles) => {
  const data = jwt.decode(token);

  localStorage.clear();

  localStorage.setItem('authorization', JSON.stringify(token));
  localStorage.setItem('user', JSON.stringify(data.user));
  localStorage.setItem('preferences', JSON.stringify(data.user.preferences));

  if (profiles) {
    localStorage.setItem('profiles', JSON.stringify(profiles));
    localStorage.setItem('acting_profile', JSON.stringify(profiles[0]));
  }
};

const getUser = () => {
  const user = localStorage.getItem('user');

  return user ? JSON.parse(user) : null;
};

export {
  authHeader,
  checkIsLoggedIn,
  checkAuthorization,
  saveAuthorization,
  getUser,
  checkAuth
};
