import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';

import Login from  './components/Login/Login';
import Inicial from  './components/Inicial/Inicial';
import PrivateRoute from './PrivateRoute';

const App = ({ history }) => (
  (
    <Router history={history}>
      <Switch>
        <Route  exact path="/login" component={Login} />

        <PrivateRoute path="/">
          <Inicial />
        </PrivateRoute>
      </Switch>

    </Router>
  )
)


export default App;
