import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import * as serviceWorker from './serviceWorker';
import { IntlProvider } from 'react-intl';

import history from './config/history';
import { flattenMessages } from './config/flattenMessages';
import ptBr from './lang/pt-BR';
import enUs from './lang/en-US';

const locale = navigator.language.split('-')[0] === 'pt' ? 'pt-BR' : 'en-US';
const langMessages = locale === 'pt-BR' ? ptBr : enUs;

ReactDOM.render(
    <IntlProvider locale={locale} messages={flattenMessages(langMessages)}>
      <App  history={history} />
    </IntlProvider>,
    document.getElementById('root')
  );
  
serviceWorker.unregister();
