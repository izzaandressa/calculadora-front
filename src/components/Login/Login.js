import React from 'react';
import { Paper, Grid, TextField, Button, FormControlLabel, Checkbox } from '@material-ui/core';
import { Face } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
      height: '100%',
      minHeight: '100%',
      display: 'flex',
      '-webkit-align-items': 'center',
      alignItems: 'center',
      '-webkit-justify-content': 'center',
      justifyContent: 'center',
      textAlign: 'center'
    },
    margin: {
        margin: theme.spacing(2),
    },
    padding: {
        padding: theme.spacing(5),
        position:'absolute',
        top:'50%',
        left:  '50%',
        transform:'translate(-50%, -50%)'
    },
    accountCicle: {
      fontSize: 80
    }
}));

const Login = () => {
    const history = useHistory();

    const onSubmit = async => {        
        localStorage.setItem('authorization', JSON.stringify('OK'));
        history.push('/');

        // const resp = await login( form ).catch(err => {
        //     console.log('error');
        //     return false;
        // });

        // if (resp.status === 200) {
        //     const { token, profiles } = resp.data;
        //     saveAuthorization(token, profiles);
        //     window.location.href = APP_URL;
        // }
    };
    const classes = useStyles();

    
    return (
        <form 
        onSubmit={async e => {
            e.preventDefault();
            onSubmit();
        }}>
            <div className={classes.root}>
                <Paper className={classes.padding}>
                    <Face className={classes.accountCicle}/>
                    <div className={classes.margin}>
                        <Grid container spacing={8} alignItems="flex-end">          
                            <Grid item md={true} sm={true} xs={true}>
                                <TextField id="username" label="Username" type="email" fullWidth autoFocus required />
                            </Grid>
                        </Grid>
                        <Grid container spacing={8} alignItems="flex-end">
                            <Grid item md={true} sm={true} xs={true}>
                                <TextField id="username" label="Password" type="password" fullWidth required />
                            </Grid>
                        </Grid>
                        <Grid container alignItems="center" justify="space-between">
                            <Grid item>
                                <FormControlLabel control={
                                    <Checkbox
                                        color="primary"
                                    />
                                } label="Remember me" />
                            </Grid>
                            <Grid item>
                                <Button disableFocusRipple disableRipple style={{ textTransform: "none" }} variant="text" color="primary">Forgot password ?</Button>
                            </Grid>
                        </Grid>
                        <Grid container justify="center" style={{ marginTop: '10px'}} >
                            <Button type="submit" variant="outlined" style={{ textTransform: "none", width: '100%' }}>Login</Button>
                        </Grid>
                    </div>
                </Paper>
            </div>
        </form>
    );
}

export default Login;