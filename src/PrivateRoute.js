import React from 'react';
import { Route, Redirect } from 'react-router-dom';

import { checkAuth} from  './utils/auth';

const PrivateRoute = ({ children, ...rest }) => (
  (
    <Route
      {...rest}
      render={({ location }) =>
      checkAuth() ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location }
            }}
          />
        )
      }
    />
  )
)


export default PrivateRoute;