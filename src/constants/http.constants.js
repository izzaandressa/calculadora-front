export const GET = 'get';
export const PUT = 'put';
export const POST = 'post';
export const DELETE = 'delete';

export const APP_URL = 'http://localhost:3000/';
export const URL_LOGIN = 'http://localhost:3000/login';
